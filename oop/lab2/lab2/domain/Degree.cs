﻿namespace lab2.domain
{
    class Degree
    {
        public int Code { get; private set; }
        public string Title { get; private set; }
        public int CreditsRequired { get; private set; }
        public int SpecialCoursesRequired { get; private set; }

        public Degree(int code, string title, int creditsRequired, int specialCoursesRequired)
        {
            Code = code;
            Title = title;
            CreditsRequired = creditsRequired;
            SpecialCoursesRequired = specialCoursesRequired;
        }
    }
}
