﻿using System;
using System.Collections.Generic;

namespace lab2.domain
{
    class Curriculum
    {
        public int Code { get; private set; }
        public DateTime CreationDate { get; private set; }
        public DateTime ConfirmationDate { get; private set; }
        public Student Human { get; private set; }
        public Degree Degree { get; private set; }
        public List<Course> Courses { get; private set; }

        public Curriculum(int code, DateTime creationDate, Student human, Degree degree)
        {
            Code = code;
            CreationDate = creationDate;
            ConfirmationDate = creationDate;
            Human = human;
            Degree = degree;
            Courses = new List<Course>();
        }

        public void AddCourse(Course course)
        {
            Courses.Add(course);
        }

        public void DeleteCourse(Course course)
        {
            Courses.Remove(course);
        }

        public bool IsAcceptable()
        {
            int degreeCredits = 0;
            int specialCourseNumber = 0;
            int matchesNumber = 0;
            foreach (var course in Courses)
            {
                foreach (var prerequisity in course.Prerequisities)
                {
                    foreach (var course2 in Courses)
                    {
                        if (prerequisity == course2.Code)
                        {
                            matchesNumber += 1;
                        }    
                    }
                }

                if (matchesNumber != course.Prerequisities.Count)
                {
                    return false;
                }
                matchesNumber = 0;

                degreeCredits += course.CalculateCreditsNumber();
                if (course.IsSpecial)
                {
                    specialCourseNumber += 1;
                }
            }

            if (Degree.CreditsRequired == degreeCredits && specialCourseNumber >= Degree.SpecialCoursesRequired)
            {
                return true;
            }
            return false;
        }
    }
}
