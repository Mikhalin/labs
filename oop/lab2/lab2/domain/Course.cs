﻿using System.Collections.Generic;

namespace lab2.domain
{
    class Course
    {
        public int Code { get; private set; }
        public string Title { get; private set; }
        public bool IsSpecial { get; private set; }
        public int LectureHours { get; private set; }
        public int PractiseHours { get; private set; }
        public bool HasExam { get; private set; }
        public bool HasCoursePaper { get; private set; }
        public  List<int> Prerequisities { get; private set; }

        public Course(int code, string title, bool isSpecial, int lectureHours, int practiseHours, bool hasExam, bool hasCoursePaper, IEnumerable<int> prerequisities)
        {
            Code = code;
            Title = title;
            IsSpecial = isSpecial;
            LectureHours = lectureHours;
            PractiseHours = practiseHours;
            HasExam = hasExam;
            HasCoursePaper = hasCoursePaper;
            Prerequisities = new List<int>(prerequisities);
        }

        public int CalculateCreditsNumber()
        {
            const double factor1 = 1.25;
            const int factor2 = 18;
            int creditsNumber = (LectureHours + (int)(factor1*PractiseHours))/factor2;
            if (HasExam)
            {
                creditsNumber += 1;
            }
            if (HasCoursePaper)
            {
                creditsNumber += 2;
            }
            return creditsNumber;
        }
    }
}
