﻿using System;

namespace lab2.domain
{
    class Student
    {
        public int ApplicationNumber { get; private set; }
        public string FullName { get; private set; }
        public DateTime BirthDate { get; private set; }

        public Student(int applicationNumber, string fullName, DateTime birthDate)
        {
            ApplicationNumber = applicationNumber;
            FullName = fullName;
            BirthDate = birthDate;
        }
    }
}
