﻿using lab2.Domain;
using lab2.Executables;
using lab2.MenuEngine;

namespace lab2
{
    class Program
    {
        static void Main()
        {
            var repository = new CurriculumRepository();
            var menu = new Menu();

            menu.AddCommand("Create curriculum", new CreateCurriculumExecutable(repository));
            menu.AddCommand("Exit", new ExitExecutable());

            menu.Run();
        }
    }
}
