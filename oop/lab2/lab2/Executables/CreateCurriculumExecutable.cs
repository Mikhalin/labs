﻿using System;
using System.Collections.Generic;
using lab2.domain;
using lab2.Domain;
using lab2.MenuEngine;

namespace lab2.Executables
{
    class CreateCurriculumExecutable : IExecutable
    {
        private readonly CurriculumRepository _repository;

        public CreateCurriculumExecutable(CurriculumRepository repository)
        {
            _repository = repository;
        }

        public void Execute()
        {
            Console.Write("Application number: ");
            var number = int.Parse(Console.ReadLine());

            Console.Write("FIO: ");
            var fio = Console.ReadLine();

            Console.Write("BirthDate: ");
            var birthDate = DateTime.Parse(Console.ReadLine());

            Console.Write("Code of curriculum: ");
            var code = int.Parse(Console.ReadLine());

            Console.Write("Creation Date: ");
            var creationDate = DateTime.Parse(Console.ReadLine());
            
            Console.WriteLine("Choose the degree:");

            var demo = new DemoData();
            foreach (var degree in demo.AllDegrees)
            {
                Console.WriteLine("{0} - {1}", degree.Code, degree.Title);
            }

            var degreeCode = int.Parse(Console.ReadLine());

            var curriculum = new Curriculum(code, creationDate, new Student(number, fio, birthDate),
                demo.AllDegrees[degreeCode - 1]);

            int courseNumber = 0;
            List<int> markedCourses = new List<int>();
            for (; courseNumber < 5; courseNumber++)
            {
                Console.WriteLine("Choose the course for your curriculum:");
                foreach (var course in demo.AllCourses)
                {
                    foreach (var mark in markedCourses)
                    {
                        if (course.Code == mark)
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            break;
                        }
                    }
                    Console.WriteLine("{0} - {1}", course.Code, course.Title);
                    Console.ResetColor();
                }
                var courseCode = int.Parse(Console.ReadLine());
                bool courseFlag = false;
                foreach (var mark in markedCourses)
                {
                    if (courseCode == mark)
                    {
                        courseFlag = true;
                        break;
                    }
                }
                if (!courseFlag)
                {
                    markedCourses.Add(courseCode);
                    curriculum.AddCourse(demo.AllCourses[courseCode - 1]);
                }
                else
                {
                    Console.WriteLine("This course has already been added.");
                    courseNumber -= 1;
                }
            }
            _repository.AddСurriculum(curriculum);
        }
    }
}
