﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab2.MenuEngine;

namespace lab2.Executables
{
    class ExitExecutable : IExecutable
    {
        public void Execute()
        {
            Environment.Exit(0);
        }
    }
}
