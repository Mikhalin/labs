﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab2.domain;

namespace lab2
{
    class DemoData
    {
        public List<Course> AllCourses = new List<Course>()
        {
            new Course(01, "analytical geometry", false, 75, 75, true, false, new List<int>()),
            new Course(02, "mathematic analysis", false, 86, 74, true, false, new List<int>()),
            new Course(03, "history", false, 64, 32, false, false, new List<int>()),
            new Course(04, "programming", false, 120, 150, true, false, new List<int>()),
            new Course(05, "cultural studies", false, 65, 45, false, false, new List<int>()),
            new Course(06, "basics of software engineering", true, 56, 84, true, false, new List<int>(){ 04 }),
            new Course(07, "philosophy", false, 92, 48, false, false, new List<int>()),
            new Course(08, "discrete mathematic", false, 86, 92, true, false, new List<int>()),
            new Course(09, "foreign language", false, 120, 18, true, false, new List<int>()),
            new Course(10, "basic electronics", false, 64, 72, false, false, new List<int>()),
            new Course(11, "computer architecture", true, 86, 86, true, false, new List<int>(){ 10 }),
            new Course(12, "politology", false, 72, 84, false, false, new List<int>()),
            new Course(13, "logic and theory of algorithms", true, 92, 92, true, false, new List<int>(){ 04, 08 }),
            new Course(14, "oop", true, 96, 98, true, false, new List<int>(){ 04 }),
            new Course(15, "linguistics", true, 120, 40, true, false, new List<int>(){ 03, 05, 09})
        };

        public List<Degree> AllDegrees = new List<Degree>()
        {
            new Degree(01, "Software Engineer", 315, 2),
            new Degree(02, "Linguist", 201, 1),
        };

        /*public void ShowAllDegrees()
        {
            foreach (var degree in AllDegrees)
            {
                Console.WriteLine("{0} - {1}", degree.Code, degree.Title);
            }
        }

        public void ShowAllCourses()
        {
            foreach (var course in AllCourses)
            {
                Console.WriteLine("{0} - {1}", course.Code, course.Title);
            }
        }
         */
    }
}
