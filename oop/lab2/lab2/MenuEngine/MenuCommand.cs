﻿namespace lab2.MenuEngine
{
    class MenuCommand
    {
        public string Title { get; private set; }
        private IExecutable _executable;

        public MenuCommand(string title, IExecutable executable)
        {
            Title = title;
            _executable = executable;
        }

        public void Execute()
        {
            _executable.Execute();
        }
    }
}
