﻿namespace lab2.MenuEngine
{
    interface IExecutable
    {
        void Execute();
    }
}
