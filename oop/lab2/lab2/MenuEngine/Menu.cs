﻿using System;
using System.Collections.Generic;

namespace lab2.MenuEngine
{
    class Menu
    {
        private readonly List<MenuCommand> _commands = new List<MenuCommand>();

        public void AddCommand(string title, IExecutable executable)
        {
            _commands.Add(new MenuCommand(title, executable));
        }

        public void Run()
        {
            while (true)
            {
                PrintCommands();
                var command = ReadCommand();
                ExecuteCommand(command);
            }
        }

        private void ExecuteCommand(int command)
        {
            _commands[command - 1].Execute();
        }

        private int ReadCommand()
        {
            Console.Write("Enter command: ");
            var commandNumber = int.Parse(Console.ReadLine());

            return commandNumber;
        }

        private void PrintCommands()
        {
            Console.WriteLine();
            var number = 1;
            foreach (var command in _commands)
            {
                Console.WriteLine("{0} - {1}", number, command.Title);
                ++number;
            }
        }
    }
}
